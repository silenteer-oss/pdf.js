import { parseQueryString } from "./ui_utils.js";
import { PDFViewerApplication } from "./viewer.js";

const frameElementId = window.frameElement ? window.frameElement.id : "";

const SupportedHookParentEvents = {
  PagesRendered: {
    id: `PAGES_RENDERED_${frameElementId}`,
    type: "PAGES_RENDERED",
    params: { pagesCount: 0 },
  },
};

const EmbeddedViewerOption = {
  hookEvent: undefined,
};

class EmbeddedViewer {
  constructor(pdfViewerAplication) {
    this.pdfViewerAplication = pdfViewerAplication;
    this.InitProps();
  }

  InitProps() {
    this.pagesCount = 0;
    this.renderedPages = [];
  }

  getConfig() {
    if (!this.appEmbeddedOption) {
      const queryString = document.location.search.substring(1);
      const params = parseQueryString(queryString);
      this.appEmbeddedOption = EmbeddedViewerOption;

      this.appEmbeddedOption.hookEvent =
        "hookEvent".toLowerCase() in params ? params.hookevent : undefined;
    }
    return this.appEmbeddedOption;
  }

  IsHookEvent() {
    return this.getConfig().hookEvent;
  }

  async InitEvents() {
    await this.pdfViewerAplication.initializedPromise;
    this.pdfViewerAplication.eventBus._on(
      "pagerendered",
      webViewerPageRendered
    );
    this.pdfViewerAplication.eventBus._on("pagesloaded", webViewerPagesLoaded);
    this.pdfViewerAplication.eventBus._on("pagesinit", webViewerPagesInit);
  }

  OnPageRendered(pageNumber, timestamp, error) {
    if (this.renderedPages.includes(pageNumber)) {
      return;
    }
    this.renderedPages.push(pageNumber);
    if (this.renderedPages.length >= this.pagesCount) {
      const event = SupportedHookParentEvents.PagesRendered;
      event.params.pagesCount = this.pagesCount;
      postMessageToParent(event.type, event.id, event.params);
    }
  }
}

const embeddedViewer = new EmbeddedViewer(PDFViewerApplication);
embeddedViewer.InitEvents();

function postMessageToParent(type, id, params) {
  if (embeddedViewer && embeddedViewer.IsHookEvent()) {
    window.parent.postMessage({ type, id, params }, "*");
  }
}
function webViewerPagesLoaded({ source, pagesCount }) {
  embeddedViewer.pagesCount = pagesCount;
}

function webViewerPagesInit({ source }) {
  embeddedViewer.InitProps();
}

function webViewerPageRendered({ pageNumber, timestamp, error }) {
  embeddedViewer.OnPageRendered(pageNumber, timestamp, error);
}

export { EmbeddedViewer, PDFViewerApplication };
